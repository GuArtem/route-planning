ARG HOME=/home/backend
ARG POETRY_VERSION=1.3.2
ARG PYTHON_VERSION=3.11-slim


FROM busybox


FROM acidrain/python-poetry:${PYTHON_VERSION}-${POETRY_VERSION} as poetry

ARG HOME
ENV POETRY_VIRTUALENVS_IN_PROJECT=true \
    PYTHONUNBUFFERED=true \
    PYTHONDONTWRITEBYTECODE=true

WORKDIR $HOME

COPY pyproject.toml poetry.lock $HOME

RUN poetry install --with dev --no-cache --no-interaction --no-ansi --no-root -vvv


FROM python:3.11-slim

ARG HOME
ENV POETRY_VIRTUALENVS_IN_PROJECT=true \
    PYTHONUNBUFFERED=true \
    PYTHONDONTWRITEBYTECODE=true \
    APP_HOME=$HOME/route_planning \
    PATH="$HOME/.venv/bin:$PATH"

WORKDIR $APP_HOME

RUN apt-get update && \
    apt-get install -y --no-install-recommends locales tini && \
    apt-get clean && \
    sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen

ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8

COPY --from=poetry $HOME/.venv $HOME/.venv
COPY --from=atkrad/wait4x:2 /usr/bin/wait4x /usr/bin/wait4x

ENTRYPOINT ["tini", "--"]
