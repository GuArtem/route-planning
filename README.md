# route_planning

This application is written to learn the nuances of working with geodata.

**Stack:**
 - Aiohttp _(fast and lightweight framework for building web applications)_
 - SqlAlchemy _(widespread multifunctional ORM)_
 - Alembic _(migrations for SqlAlchemy models)_
 - PostgreSQL _(widespread relational database)_
 - PostGIS _(PostgreSQL plugin for working with geodata)_
 - Geoalchemy2 _(library for supporting geodata in models SqlAlchemy)_
 - Pydantic _(fast and lightweight data validation library)_

## Installation
Clone this project.

## Usage

You can change the settings in the `app/config/config.yml`.

Add `.env` file with custom environment values and run:
```bash
$ docker-compose up -d
```

## API documentation

You can look at the swagger API documentation at the url `/api/doc`
