from aiohttp import web

from app.auth.urls import setup_routes as setup_auth_routes
from app.map_routes.urls import setup_routes as setup_map_routes


def setup_routes(application: web.Application) -> web.Application:
    application = setup_auth_routes(application)
    application = setup_map_routes(application)

    return application
