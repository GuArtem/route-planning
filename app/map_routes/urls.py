from aiohttp import web

from app.map_routes import api


def setup_routes(application: web.Application) -> web.Application:
    application.router.add_view("/api/points.list", api.ListPointsApiView)
    application.router.add_view("/api/routes.list", api.ListRoutesApiView)
    application.router.add_view("/api/routes.add", api.CreateRouteApiView)

    return application
