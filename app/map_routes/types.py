from typing import TypeAlias, TypedDict

Latitude: TypeAlias = float
Longitude: TypeAlias = float
PointId: TypeAlias = str
RouteId: TypeAlias = str
CreateRouteData: TypeAlias = dict[str, str | list[str]]


class RouteDict(TypedDict):
    title: str
    points: list[PointId]
