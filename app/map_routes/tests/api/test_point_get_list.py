from typing import Any

import pytest
from aiohttp.test_utils import TestClient

from app.map_routes.routes.serializers import PointSerializer
from app.map_routes.tests.api.types import HttpHeaders

pytestmark = pytest.mark.asyncio


@pytest.mark.database_access
@pytest.mark.api
async def test_point_list__access_allowed(
    http_client: TestClient,
    json_base_headers: HttpHeaders,
):
    response = await http_client.get(
        "/api/points.list",
        headers=json_base_headers,
    )

    assert response.status == 200


@pytest.mark.database_access
@pytest.mark.api
async def test_point_list__one_point_structure(
    http_client: TestClient,
    json_base_headers: HttpHeaders,
):
    response = await http_client.get(
        "/api/points.list",
        headers=json_base_headers,
    )

    result = await response.json()
    assert len(result.get("result", [])) > 0

    one_point: dict[str, Any] = result["result"][0]
    required_keys = sorted(list(PointSerializer.__fields__.keys()))
    assert sorted(list(one_point.keys())) == required_keys
