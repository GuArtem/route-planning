import pytest
from aiohttp.test_utils import TestClient

from app.map_routes.tests.api.types import HttpHeaders
from app.map_routes.types import CreateRouteData

pytestmark = pytest.mark.asyncio

create_route_url = "/api/routes.add"


@pytest.mark.database_access
@pytest.mark.api
async def test_route_create__access_allowed(
    http_client: TestClient,
    one_route_data: CreateRouteData,
    json_base_headers: HttpHeaders,
):
    response = await http_client.post(
        create_route_url,
        json=one_route_data,
        headers=json_base_headers,
    )

    assert response.status == 200


@pytest.mark.database_access
@pytest.mark.api
async def test_route_create__structure_success(
    http_client: TestClient,
    one_route_data: CreateRouteData,
    json_base_headers: HttpHeaders,
):
    response = await http_client.post(
        create_route_url,
        json=one_route_data,
        headers=json_base_headers,
    )

    response_data = await response.json()
    route = response_data.get("result")
    assert route is not None

    required_fields = ["id", "title"]
    assert sorted(list(route.keys())) == required_fields


@pytest.mark.database_access
@pytest.mark.api
@pytest.mark.parametrize(
    "new_route_data",
    [
        {"title": "New Route"},
        {"title": ""},
        {"points": ["123456", "654321", "qwerty"]},
        {"points": []},
        {"title": "", "points": ["123456", "654321", "qwerty"]},
        {"title": "New Route", "points": []},
        {"title": "New Route", "points": ["123456", "654321", "qwerty"]},
        {"title": "New Route", "points": [123456, 654321]},
        {"title": 123, "points": ["123456", "654321", "qwerty"]},
        {"title": "New Route", "points": ""},
        {"title": 123, "points": ""},
    ],
)
async def test_route_create__validation_error(
    http_client: TestClient,
    json_base_headers: HttpHeaders,
    new_route_data: CreateRouteData,
):
    response = await http_client.post(
        create_route_url,
        json=new_route_data,
        headers=json_base_headers,
    )

    assert response.status == 400
