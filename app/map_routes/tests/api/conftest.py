from random import randint

from pytest import fixture
from sqlalchemy import Select, delete, func
from sqlalchemy.ext.asyncio import AsyncSession

from app.map_routes.models import Point, Route, RoutesPoints
from app.map_routes.types import CreateRouteData
from app.store.database.accessor import PostgresAccessor


@fixture
async def one_route_data(application_for_tests) -> CreateRouteData:
    db_accessor: PostgresAccessor = application_for_tests["db"]
    async with AsyncSession(db_accessor.engine) as session:
        point_ids_query = (
            Select(Point.id).order_by(func.random()).limit(randint(3, 100))
        )
        points: list[str] = [
            point.id for point in await session.execute(point_ids_query)
        ]
    yield {"title": "new Route (created in test)", "points": points}

    async with AsyncSession(db_accessor.engine) as session:
        route_ids = Select(Route.id).where(
            Route.title.like("%(created in test)%"),
        )

        delete_assoc_query = delete(RoutesPoints).where(
            RoutesPoints.route_id.in_(route_ids),
        )

        delete_routes_query = delete(Route).where(Route.id.in_(route_ids))

        await session.execute(delete_assoc_query)
        await session.execute(delete_routes_query)
        await session.commit()


@fixture
def json_base_headers() -> dict[str, str]:
    return {"content-type": "application/json; utf-8"}
