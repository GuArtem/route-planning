from typing import TypeAlias

HttpHeaders: TypeAlias = dict[str, str]
