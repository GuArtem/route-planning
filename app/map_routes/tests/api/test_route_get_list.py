from typing import Any

import pytest
from aiohttp.test_utils import TestClient

from app.map_routes.routes.serializers import (
    RoutePointSerializer,
    RouteWithPointsSerializer,
)
from app.map_routes.tests.api.types import HttpHeaders

pytestmark = pytest.mark.asyncio


@pytest.mark.database_access
@pytest.mark.api
async def test_route_list__access_allowed(
    http_client: TestClient,
    json_base_headers: HttpHeaders,
):
    response = await http_client.get(
        "/api/routes.list",
        headers=json_base_headers,
    )

    assert response.status == 200


@pytest.mark.database_access
@pytest.mark.api
async def test_route_list__one_route_structure(
    http_client: TestClient,
    json_base_headers: HttpHeaders,
):
    response = await http_client.get(
        "/api/routes.list",
        headers=json_base_headers,
    )

    result = await response.json()
    assert len(result.get("result", [])) > 0

    one_route: dict[str, Any] = result["result"][0]
    required_keys = list(RouteWithPointsSerializer.__fields__.keys()).sort()
    assert list(one_route.keys()).sort() == required_keys

    one_route_point = one_route["points"][0]
    required_keys = sorted(list(RoutePointSerializer.__fields__.keys()))
    assert sorted(list(one_route_point.keys())) == required_keys
