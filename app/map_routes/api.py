from typing import Any, TypeAlias

from aiohttp import web
from pydantic import ValidationError

from app.map_routes.points.get import get_list as get_points
from app.map_routes.points.serializers import PointSerializer
from app.map_routes.routes.create import InsertRouteError
from app.map_routes.routes.create import insert_one as insert_one_route
from app.map_routes.routes.get import get_list as get_routes
from app.map_routes.routes.serializers import (
    InsertRouteSerializer,
    RouteSerializer,
    RouteWithPointsSerializer,
)
from app.map_routes.types import CreateRouteData

ErrorCreateRouteData: TypeAlias = dict[str, list[dict[str, Any]]]


class ListPointsApiView(web.View):
    async def get(self) -> web.Response:
        """
        ---
        description: This is the endpoint for getting a list of geopoints
         with coordinates
        tags:
        - Route
        produces:
        - application/json
        responses:
            "200":
                description: successful operation. Return point list
        """
        points = await get_points(self.request.app["db"])
        data = [PointSerializer.from_orm(val).dict() for val in points]

        return web.json_response(data={"result": data})


class ListRoutesApiView(web.View):
    async def get(self) -> web.Response:
        """
        ---
        description: This is the endpoint for getting a list of routes with a list
         of their geopoints.
        tags:
        - Route
        produces:
        - application/json
        responses:
            "200":
                description: successful operation. Return route list
        """
        routes = await get_routes(self.request.app["db"])
        data = [RouteWithPointsSerializer.from_orm(val).dict() for val in routes]

        return web.json_response(data={"result": data})


class CreateRouteApiView(web.View):
    create_serializer = InsertRouteSerializer

    async def post(self) -> web.Response:
        """
        ---
        description: This is the endpoint for creating route
        tags:
          - Route
        produces:
          - application/json
        parameters:
          - in: body
            name: title
            required: true
            type: string
            description: New route title
          - in: body
            name: points
            required: true
            description: List of Point ids
            schema:
              type: array
              items:
                type: string
        responses:
            "200":
                description: successful operation. Return new route.
            "400":
                description: error operation. Return error details.
        """
        is_valid, data = self.serialize(await self.request.json())
        if not is_valid:
            return web.json_response(data=data, status=400)

        try:
            route = await insert_one_route(self.request.app["db"], data)
        except InsertRouteError as ex:
            return web.json_response(status=400, data={"detail": ex.message})

        return web.json_response(
            data={"result": RouteSerializer.from_orm(route).dict()},
        )

    def serialize(
        self,
        request_data: dict[str, Any],
    ) -> tuple[bool, CreateRouteData | ErrorCreateRouteData]:
        try:
            serializer = self.create_serializer(**request_data)
        except ValidationError as ex:
            return False, {"detail": ex.errors()}
        else:
            return True, serializer.dict()
