from datetime import datetime

from geoalchemy2.types import Geometry
from sqlalchemy import TIMESTAMP, ForeignKey, text
from sqlalchemy.orm import Mapped, mapped_column, query_expression, relationship
from sqlalchemy.types import Boolean, Integer, String

from app.store.database.models import Base


class RoutesPoints(Base):
    __tablename__ = "routes_points_m2m"

    route_id: Mapped[str] = mapped_column(ForeignKey("route.id"), primary_key=True)
    point_id: Mapped[str] = mapped_column(ForeignKey("point.id"), primary_key=True)
    start: Mapped[bool] = mapped_column(Boolean, default=False)
    end: Mapped[bool] = mapped_column(Boolean, default=False)
    order: Mapped[int] = mapped_column(Integer, nullable=False)

    point: Mapped["Point"] = relationship(
        "Point",
        back_populates="routes",
        innerjoin=True,
    )
    route: Mapped["Route"] = relationship(
        "Route",
        back_populates="points",
        innerjoin=True,
    )

    def __repr__(self) -> str:
        return f"<RoutesPoints: #{self.route_id=!s} <-> #{self.point_id=!r}>"


class Point(Base):
    __tablename__ = "point"

    id: Mapped[str] = mapped_column(
        String(36),
        server_default=text("fn_uuid_time_ordered()"),
        primary_key=True,
    )
    title: Mapped[str] = mapped_column(String(length=100), nullable=False)
    coordinates: Mapped[Geometry] = mapped_column(
        Geometry("POINT", name="GEOMETRY", srid=4326),
        nullable=False,
    )
    created_at: Mapped[datetime] = mapped_column(
        TIMESTAMP(True),
        server_default=text("NOW()"),
    )

    routes: Mapped[list["RoutesPoints"]] = relationship(
        "RoutesPoints",
        back_populates="point",
    )

    lat: Mapped[float] = query_expression()
    lon: Mapped[float] = query_expression()

    def __repr__(self) -> str:
        return f"<Point: #{self.id!s}  {self.title!r}>"


class Route(Base):
    __tablename__ = "route"

    id: Mapped[str] = mapped_column(
        String(36),
        server_default=text("fn_uuid_time_ordered()"),
        primary_key=True,
    )
    title: Mapped[str] = mapped_column(String(100), nullable=False)
    created_at: Mapped[datetime] = mapped_column(
        TIMESTAMP(True),
        server_default=text("NOW()"),
    )

    points: Mapped[list["RoutesPoints"]] = relationship(
        "RoutesPoints",
        back_populates="route",
        order_by="asc(RoutesPoints.order)",
        innerjoin=True,
    )

    def __repr__(self) -> str:
        return f"<Route: {self.title!r}>"
