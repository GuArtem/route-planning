from collections import namedtuple

from pydantic import BaseModel, validator
from pydantic.error_wrappers import ValidationError

from app.map_routes.models import RoutesPoints
from app.map_routes.types import PointId, RouteId

PointTuple = namedtuple("Point", "id title lat lon start end order")
CoordinatesTuple = namedtuple("Coordinates", "lat lon")


class CoordinatesSerializer(BaseModel):
    lat: float
    lon: float

    class Config:
        orm_mode = True


class PointSerializer(CoordinatesSerializer):
    id: PointId
    title: str

    class Config:
        orm_mode = True


class RoutePointSerializer(PointSerializer):
    start: bool
    end: bool
    order: int

    class Config:
        orm_mode = True


class RouteSerializer(BaseModel):
    id: RouteId
    title: str

    class Config:
        orm_mode = True


class RouteWithPointsSerializer(RouteSerializer):
    points: list[RoutePointSerializer]

    class Config:
        orm_mode = True

    @validator("points", pre=True)
    def extract_points_from_relationships(
        cls,
        values: list[RoutesPoints],
    ) -> list[PointTuple]:
        points: list[PointTuple] = []
        for relationship in values:
            result_point = PointTuple(
                id=relationship.point.id,
                title=relationship.point.title,
                lat=relationship.point.lat,
                lon=relationship.point.lon,
                start=relationship.start,
                end=relationship.end,
                order=relationship.order,
            )
            points.append(result_point)
        return points


class InsertRouteSerializer(BaseModel):
    title: str
    points: list[PointId]

    @validator("title")
    def title_must_be_filled(cls, value: str) -> str:
        if len(value) == 0:
            raise ValidationError("Must be filled")
        return value

    @validator("points")
    def points_must_be_filled(cls, point_list: list[str]) -> list[str]:
        if len(point_list) == 0:
            raise ValidationError("Must be filled")
        return point_list
