from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.map_routes.models import Point, Route, RoutesPoints
from app.map_routes.types import RouteDict
from app.store.database.accessor import PostgresAccessor


class InsertRouteError(ValueError):
    message = "Invalid Route data"

    def __int__(self, message: str | None):
        if message is not None:
            self.message = message


async def insert_one(
    db_accessor: PostgresAccessor,
    inserting_route: RouteDict,
) -> Route:
    async with AsyncSession(db_accessor.engine) as session:
        route = Route(title=inserting_route["title"])

        points_query = select(Point).where(Point.id.in_(inserting_route["points"]))
        for index, point in enumerate(await session.execute(points_query), 1):
            relationship = get_relationship_to_point(
                point=point[0],
                num_points_in_route=len(inserting_route["points"]),
                index_in_route=index,
            )
            route.points.append(relationship)

        if len(route.points) == 0:
            raise InsertRouteError("Invalid points")

        session.add(route)
        await session.commit()
        await session.refresh(route)

        return route


def get_relationship_to_point(
    point: Point,
    num_points_in_route: int,
    index_in_route: int,
) -> RoutesPoints:
    relationship = RoutesPoints(point_id=point.id, order=index_in_route)
    if index_in_route == 1:
        relationship.start = True
    elif index_in_route == num_points_in_route:
        relationship.end = True
    return relationship
