from collections.abc import Sequence

from sqlalchemy import Select, func, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload, with_expression

from app.map_routes.models import Point, Route, RoutesPoints
from app.store.database.accessor import PostgresAccessor


def get_list_query() -> Select:
    return select(Route).options(
        selectinload(Route.points)
        .joinedload(RoutesPoints.point)
        .options(
            with_expression(Point.lat, func.st_x(Point.coordinates).label("lat")),
            with_expression(Point.lon, func.st_y(Point.coordinates).label("lon")),
        )
    )


async def get_list(db_accessor: PostgresAccessor) -> Sequence[Route]:
    async with AsyncSession(db_accessor.engine) as session:
        return (await session.scalars(get_list_query())).unique().all()
