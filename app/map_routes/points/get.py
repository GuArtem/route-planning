from sqlalchemy import Result, func, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.map_routes.models import Point
from app.store.database.accessor import PostgresAccessor


async def get_list(db_accessor: PostgresAccessor) -> Result[list[Point]]:
    async with AsyncSession(db_accessor.engine) as session:
        query = select(
            Point.id,
            Point.title,
            func.st_x(Point.coordinates).label("lat"),
            func.st_y(Point.coordinates).label("lon"),
        )
        return await session.execute(query)
