from pydantic import BaseModel

from app.map_routes.types import Latitude, Longitude, PointId


class PointSerializer(BaseModel):
    id: PointId
    title: str
    lat: Latitude
    lon: Longitude

    class Config:
        orm_mode = True
