import pytest
from aiohttp import web
from aiohttp.test_utils import TestClient

from app.initialize import setup_app
from app.settings import tests_config


@pytest.fixture(scope="module")
def anyio_backend():
    return "asyncio"


@pytest.fixture
def application_for_tests() -> web.Application:
    app = web.Application()
    return setup_app(app, app_settings=tests_config)


@pytest.fixture
def http_client(
    event_loop,
    aiohttp_client,
    application_for_tests: web.Application,
) -> TestClient:
    return event_loop.run_until_complete(aiohttp_client(application_for_tests))
