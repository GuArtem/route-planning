from typing import TypedDict

from app.store.database.types import PostgresConfig


class CommonConfig(TypedDict):
    port: int
    debug: bool


class Config(TypedDict):
    common: CommonConfig
    postgres: PostgresConfig
