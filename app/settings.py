import os
import re
from functools import partial
from pathlib import Path

import yaml
from dotenv import load_dotenv

from app.types import Config

BASE_DIR = Path(__file__).parent.parent
APP_DIR = BASE_DIR / "app"
CONFIG_PATH = APP_DIR / "config" / "config.yml"


def get_config(path: Path, env_file_name: str = ".env") -> Config:
    load_env_file(BASE_DIR / env_file_name)
    with open(path) as conf_file:
        return yaml.load(conf_file, Loader=_get_safe_loader())


def load_env_file(env_file: Path) -> None:
    if env_file.exists():
        load_dotenv(env_file)
    else:
        load_dotenv(BASE_DIR / ".env.default")


def _get_safe_loader(
    env_tag: str = "!ENV",
    env_pattern: re.Pattern = re.compile(r".*?\${(\w+)}.*?"),
) -> type[yaml.SafeLoader]:
    """
    Get yaml SafeLoader which can resolve environment variables in yaml data
    The environment variables must have !ENV before them and be in this format
    to be parsed: ${VAR_NAME}.

    E.g.:
    database:
        host: !ENV ${HOST}
        port: !ENV ${PORT}
    app:
        log_path: !ENV '/var/${LOG_PATH}'
        something_else: !ENV '${AWESOME_ENV_VAR}/var/${A_SECOND_AWESOME_VAR}'
    """

    loader = yaml.SafeLoader

    loader.add_implicit_resolver(tag=env_tag, regexp=env_pattern, first=None)

    loader.add_constructor(
        env_tag,
        partial(replace_env_variables, pattern=env_pattern),
    )

    return loader


def replace_env_variables(
    loader: yaml.Loader,
    node: yaml.nodes.Node,
    pattern: re.Pattern,
) -> str:
    """
    Replace the environment variable name to the environment variable value
    in the node's value
    """

    value: str = loader.construct_scalar(node)
    if (match := pattern.findall(value)) is None:
        return value

    full_value = value
    for name in match:
        full_value = full_value.replace(
            f"${{{name}}}",
            os.environ.get(name, name),
        )
    return full_value


config: Config = get_config(CONFIG_PATH)
tests_config: Config = get_config(CONFIG_PATH, env_file_name=".env.tests")
