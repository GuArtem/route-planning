from aiohttp import web

from app.routes import setup_routes
from app.store.database.accessor import PostgresAccessor
from app.types import Config


def setup_config(application: web.Application, conf: Config) -> web.Application:
    application["config"] = conf

    return application


def setup_accessors(
    application: web.Application,
    accessor: PostgresAccessor,
) -> web.Application:
    app_with_accessor = accessor.setup(application)
    app_with_accessor["db"] = accessor

    return app_with_accessor


def setup_app(
    application: web.Application,
    app_settings: Config,
) -> web.Application:
    configured_app = setup_config(application, app_settings)
    app_with_accessors = setup_accessors(configured_app, PostgresAccessor())
    app_with_routes = setup_routes(app_with_accessors)

    return app_with_routes
