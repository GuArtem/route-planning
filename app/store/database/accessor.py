from aiohttp import web
from sqlalchemy.ext.asyncio import AsyncEngine, create_async_engine

from app.types import Config


class PostgresAccessor:
    def __init__(self):
        self.engine: AsyncEngine | None = None

    def setup(self, application: web.Application) -> web.Application:
        application.on_startup.append(self._on_connect)
        application.on_cleanup.append(self._on_disconnect)

        return application

    async def _on_connect(self, application: web.Application) -> None:
        config: Config = application["config"]
        self.engine = create_async_engine(
            config["postgres"]["dsn"],
            echo=config["common"]["debug"],
        )

    async def _on_disconnect(self, _) -> None:
        if self.engine is not None:
            await self.engine.dispose()
