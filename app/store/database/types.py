from typing import TypedDict


class PostgresConfig(TypedDict):
    dsn: str
