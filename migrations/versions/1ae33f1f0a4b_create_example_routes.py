"""create example routes

Revision ID: 1ae33f1f0a4b
Revises: da7585512cde
Create Date: 2023-03-17 18:45:09.322240

"""
from random import randint

from alembic import op
import sqlalchemy as sa
from sqlalchemy.orm.session import Session
from sqlalchemy.sql.expression import func

from app.map_routes.models import Point, Route, RoutesPoints


# revision identifiers, used by Alembic.
revision = '1ae33f1f0a4b'
down_revision = 'da7585512cde'
branch_labels = None
depends_on = None


def upgrade() -> None:
    with Session(bind=op.get_bind()) as session:
        for route_num in range(50):
            num_points = randint(2, 100)
            route = Route(title=f"Route #{route_num}")

            point_stmt = sa.select(Point).order_by(func.random()).limit(num_points)
            for index, point_res in enumerate(session.execute(point_stmt), start=1):
                point = point_res[0]
                associate = RoutesPoints(order=index, point_id=point.id)
                if index == 1:
                    associate.start = True
                elif index == num_points:
                    associate.end = True
                route.points.append(associate)

            session.add(route)
            session.commit()


def downgrade() -> None:
    op.execute(sa.text("TRUNCATE table routes_points_m2m;"))
    op.execute(sa.text("TRUNCATE table route CASCADE;"))
