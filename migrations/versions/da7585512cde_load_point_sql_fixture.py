"""load point sql fixture

Revision ID: da7585512cde
Revises: d5d01b62c0de
Create Date: 2023-03-17 16:50:12.008462

"""
from pathlib import Path
from typing import Iterable

from alembic import op
import sqlalchemy as sa

from app.settings import BASE_DIR


# revision identifiers, used by Alembic.
revision = 'da7585512cde'
down_revision = 'd5d01b62c0de'
branch_labels = None
depends_on = None


def _load_points(fixture_path: Path) -> Iterable[str]:
    with open(fixture_path) as sql_file:
        yield sql_file.readline()


def upgrade() -> None:
    if not (fixture_path := BASE_DIR / "fixtures" / "point_fixture.sql").exists():
        return
    for sql_query in _load_points(fixture_path):
        op.execute(sa.text(sql_query))


def downgrade() -> None:
    op.execute(sa.text("TRUNCATE table point RESTART IDENTITY CASCADE;"))
