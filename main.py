from aiohttp import web
from aiohttp_swagger import setup_swagger

from app.initialize import setup_app
from app.settings import config

app: web.Application = web.Application()


if __name__ == "__main__":
    initialized_app = setup_app(app, app_settings=config)
    setup_swagger(initialized_app)
    web.run_app(initialized_app, port=config["common"]["port"])
